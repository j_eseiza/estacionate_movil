package com.example.ale.estacionate

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun onClick(view: View){
        val i = Intent(applicationContext, RegisterActivity::class.java)
        startActivity(i)
    }

    fun goHome(view: View){
        val i = Intent(applicationContext, HomeActivity::class.java)
        startActivity(i)
    }
}

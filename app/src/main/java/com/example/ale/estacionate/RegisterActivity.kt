package com.example.ale.estacionate

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }
    fun onClick(view: View){
        val i = Intent(applicationContext, CheckRegisterActivity::class.java)
        startActivity(i)
    }
    fun goBack(view: View){
        val i = Intent(applicationContext, LoginActivity::class.java)
        startActivity(i)
    }

}
